class mpwar_gabriel {

# Puppet manifest for my PHP dev machine
# Edit local /etc/hosts files to resolve some hostnames used on your application.
host { 'localhost':
	ensure => 'present',
	target => '/etc/hosts',
	ip => '127.0.0.1',
	host_aliases => ['mysql', 'mysql1', 'memcached1']
}


# Miscellaneous packages.
$misc_packages = ['vim-enhanced','telnet','zip','unzip','git','screen','libssh2','libssh2-devel','gcc','gcc-c++','autoconf','automake']

package { $misc_packages: ensure => latest }


include stdlib
#include libvirt

# FIREWALL --------------------------------------
class { 'iptables': }

#class { 'firewall': }


# APACHE ----------------------------------------
class {'apache':  }

#class {'apache::mod::php': }


apache::vhost { 'mympwar.prod':
	port       	=> '80',
	docroot		=> '/var/www/prod',
	docroot_owner	=> 'vagrant',	
        docroot_group	=> 'vagrant',	
}

apache::vhost { 'mympwar.dev':
	port       	=> '80',
	docroot		=> 'var/www/dev',
	docroot_owner	=> 'vagrant',
	docroot_group	=> 'vagrant',
}



class {'crea_index_php': }


# PUPPI -----------------------------------------
class { 'puppi':
  install_dependencies => false,
}



# PHP ====================================================

#$php_version = '56'

$php_version = '55'

# remi_php55 requires the remi repo as well

if $php_version == '55' {

  $yum_repo = 'remi-php55'

  include ::yum::repo::remi_php55

}

# remi_php56 requires the remi repo as well

elsif $php_version == '56' {

  $yum_repo = 'remi-php56'

  ::yum::managed_yumrepo { 'remi-php56':

    descr          => 'Les RPM de remi pour Enterpise Linux $releasever - $basearch - PHP 5.6',

    mirrorlist     => 'http://rpms.famillecollet.com/enterprise/$releasever/php56/mirror',

    enabled        => 1,

    gpgcheck       => 1,

    gpgkey         => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi',

    gpgkey_source  => 'puppet:///modules/yum/rpm-gpg/RPM-GPG-KEY-remi',

    priority       => 1,

  }

}

# version 5.4

elsif $php_version == '54' {

  $yum_repo = 'remi'

  include ::yum::repo::remi

}


class { 'php':

  version => 'latest',

  require => Yumrepo[$yum_repo]

}





#class { 'php':
#  version => '5.5.0',
#}


#PHP useful packages. --------------------------
#php::ini { '/etc/php.ini':
#	display_errors	=> 'On',
#	short_open_tag	=> 'On',
#	memory_limit 	=> '512M',
#	date_timezone	=> 'Europe/Madrid',
#	html_errors	=> 'On'
#}
# include php::cli
# include php::mod_php5


#php::module { [ 'devel', 'pear', 'mysql', 'mbstring', 'xml', 'gd', 'tidy', 'pecl-apc', 'pecl-memcache', 'soap' ]: }

# MYSQL ----------------------------------------
class { "mysql": }

#class { "mysql": }
mysql::grant { 'mympwar':
      mysql_user => 'myusername',
      mysql_password => 'mypassword',
}

mysql::grant { 'mpwar_test':
      mysql_user => 'myusername',
      mysql_password => 'mypassword',
}


#mysql::db { 'mpwar_test':
#  user     => 'admin',
#  password => 'admin',
#  host     => 'localhost',
#  grant    => ['SELECT', 'UPDATE'],
#}


# EPEL -----------------------------------------
#class { 'epel':}

# MEMCACHED ------------------------------------
class { 'memcached': }

class { 'ntp': }

#class { 'yum':
#}

}

